package com.pawelbanasik;

public class SimpleEx {

	public int addElementsDividedBySeven(int[][] arr) {

		int sum = 0;
		for (int i = 0; i < arr.length; i++) {

			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] % 7 == 0) {
					sum += arr[i][j];

				}

			}
		}
		return sum;
	}

	public int multiplyElements(int[][] arr) {

		int iloczyn = 1;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				iloczyn *= arr[i][j];

			}
		}
		return iloczyn;

	}

	public int multiplyEvenElements(int[][] arr) {

		int iloczyn = 1;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] % 2 == 0) {
					iloczyn *= arr[i][j];
				}

			}
		}
		return iloczyn;

	}

	public int multiplyOddElements(int[][] arr) {

		int iloczyn = 1;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] % 2 != 0) {
					iloczyn *= arr[i][j];
				}

			}
		}
		return iloczyn;

	}

	public int multiplyElementsDividedByThree(int[][] arr) {

		int iloczyn = 1;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				if (arr[i][j] % 3 == 0) {
					iloczyn *= arr[i][j];
				}

			}
		}
		return iloczyn;

	}

	// Zadanie 6 �wiczenia wstepne
	public int find2DArrayMin(int[][] arr) {

		// 1 sposob
		// int m = Integer.MAX_VALUE;
		//
		// for (int i = 0; i < arr.length; i++) {
		// for (int j = 0; j < arr[i].length; j++) {
		//
		// if (m > arr[i][j]) {
		// m = arr[i][j];
		// }
		// }
		//
		//
		// }
		// return m;

		// sposob 2
		int min = arr[0][0];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (arr[i][j] < min) {

					min = arr[i][j];
				}

			}
		}
		return min;
	}

	// Zadanie 7 �wiczenia wstepne
	public int find2DArrayMax(int[][] arr) {

		int m = Integer.MIN_VALUE;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				if (m < arr[i][j]) {
					m = arr[i][j];
				}
			}

		}
		return m;

	}

	// Zadanie 8 �wiczenia wstepne
	public int[] sumEvenNumbersInRows(int[][] m) {
		int[] array = new int[m.length];

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {

				if (m[i][j] % 2 == 0) {
					array[i] += m[i][j];
				}
			}

		}

		for (int element : array) {
			System.out.println(element);
		}
		return array;

	}

}
