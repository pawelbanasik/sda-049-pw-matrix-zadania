package com.pawelbanasik;

public class Matrix {

	public void print2DArray(int[][] m) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				System.out.print(m[i][j]);

			}
			System.out.println();
		}
	}

	// Zadanie 2 matrix
	public int[][] identityMatrix(int[][] m) {

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {

				m[i][j] = 1;

			}
		}
		return m;

	}

	// Zadanie 3 Matrix
	public int[][] indexedMatrix(int[][] m) {

		int count = 0;

		for (int i = 0; i < m.length; i++) {

			for (int j = 0; j < m[i].length; j++) {

				count++;
				m[i][j] = count;

			}

		}
		return m;

	}

	// Zadanie 4 Matrix
	public boolean isEqualDimension(int[][] a, int[][] b) {

		int counta = 0;
		int countb = 0;

		for (int i = 0; i < a.length; i++) {

			for (int j = 0; j < a[i].length; j++) {

				counta++;

			}

		}

		for (int i = 0; i < b.length; i++) {

			for (int j = 0; j < b[i].length; j++) {

				countb++;

			}

		}

		if (counta == countb && a.length == b.length) {
			System.out.println("Wymiary r�wne");
			return true;

		} else {
			System.out.println("Wymiary nierowne");
			return false;
		}

	}

	// Zadanie 5 Matrix
	public int[][] addMatrix(int[][] a, int[][] b) {

		// int [][]c = new int[a.length][a[0].length];

		print2DArray(a);
		print2DArray(b);
		isEqualDimension(a, b);

		for (int i = 0; i < a.length; i++) {

			for (int j = 0; j < a[i].length; j++) {

				// c[i][j] = a[i][j] + b[i][j];
				a[i][j] = a[i][j] + b[i][j];
			}

		}
		return a;

		// return c;

	}

	// Zadanie 6 Matrix
	public int[][] substractMatrix(int[][] a, int[][] b) {

		// int [][]c = new int[a.length][a[0].length];

		print2DArray(a);
		print2DArray(b);
		isEqualDimension(a, b);

		for (int i = 0; i < a.length; i++) {

			for (int j = 0; j < a[i].length; j++) {

				// c[i][j] = a[i][j] + b[i][j];
				a[i][j] = a[i][j] - b[i][j];
			}

		}
		return a;

		// return c;

	}

	// Zadanie 7 Matrix
	public int[][] multiplicateMatrix(int[][] m, int n) {

		print2DArray(m);
		System.out.println("Liczba przez ktora mnoze: " + n);

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {

				m[i][j] = m[i][j] * n;
			}
		}
		print2DArray(m);
		return m;
	}

	// Zadanie 8 Matrix
	public int[][] transpose(int[][] m) {
		int a = m.length;
		int b = m[0].length;

		int[][] newArray = new int[a][b];
		print2DArray(m);

		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				newArray[i][j] = m[j][i];
			}

		}
		print2DArray(newArray);
		return newArray;

	}

	// Zadanie 9 Matrix
	public boolean isSymetric(int[][] a) {

		boolean isSymmetric = false;

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				if (a.length != a[i].length) {
					isSymmetric = false;
					break;

				}
			}
		}

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {

				if (a[i][j] != a[j][i]) {

					isSymmetric = false;
					break;

				} else {
					isSymmetric = true;

				}
			}
		}
		if (isSymmetric == true) {
			System.out.println("Matrix is symmetric.");
		} else {
			System.out.println("Matrix is not symmetric.");
		}

		return isSymmetric;

	}
}
